<p class="info" style="background: #c4351e"><span class="fa fa-exclamation-triangle"></span>&nbsp;&nbsp;These docs are no longer updated! Visit our new docs <a href="https://neverbounce.com/help/api/getting-started-with-the-api/" style="color: white; text-decoration: underline;">here</a>.</p>

Getting Started
===============
The NeverBounce API is a common RESTful API. All requests are performed via an HTTPS connection and responses are returned as JSON. This widely accepted format allows for easy use with the most common programming languages and development kits while remaining portable, expandable, and secure.
Below you will find a description of the various endpoints, the parameters they accept, and the responses they send.

``` bash
https://api.neverbounce.com/v1
```

Authentication
--------------
Each request to the NeverBounce API requires an APP ID and an API key, which can be generated in your Neverbounce.com user dashboard. These credentials should not be shared and should always be kept secure. Keep in mind all requests must be made via HTTPS.
Your unique credentials should be sent with the request as POST data.

``` bash
$ curl https://api.neverbounce.com/v1/<endpoint>
    -d key=<API_KEY>
    -d app_id=<APP_ID>
```

Error Handling
--------------
Any response coming from the NeverBounce API will include a ‘success’ property. If no errors occured during the process, this property will be set to TRUE. If this property is set to FALSE this indicates that an error has occured.
If an error has occurred, an error_code and error_msg will also be in the response.

>####Example Request
The following example will demonstrate the error handling. Note the exclusion of our API Key.

``` bash
$ curl https://api.neverbounce.com/v1/single
    -d app_id=<APP_ID>
    -d email=support@neverbounce.com
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| success | bool | FALSE. Indicates failure |
| error_code | int | A general error code indicating reason for failure |
| error_msg | string | A human-readable explanation/indication of the error. |

>####Example Response

``` javascript
{
    "success":false,
    "error_code":2,
    "error_msg":"Missing required parameter 'key'",
    "execution_time":2.288818359375e-5
}
```

###Error Codes
| Error Code | Definition |
| -- | -- |
| 0	| Internal API error |
| 1	| Authorization failure. Check that your APP ID and API Key are correct. |
| 2 | Malformed request. Refer to documentation below for proper formatting |
| 3 | Invalid Job |
| 4 | Insufficient credits. You do not have enough credits in your account to complete the request. |

Methods
=======
Single
------
This endpoint is used to validate single email addresses.

>####Example Request

``` bash
$ curl https://api.neverbounce.com/v1/single
    -d key=<API_KEY>
    -d app_id=<APP_ID>
    -d email=<EMAIL>
```

### Request Properties
| Property | Type | Definition |
| -- | -- | -- |
| app_id | string | API Application ID |
| key | string | API Key |
| email | string | The email address to validate |
| name_f (optional) | string | The first name associated with this email address |
| name_l (optional) | string | The last name associated with this email address |

>####Example Response
``` javascript
{
    "success":true,
    "result":0,
    "result_detail":0,
    "execution_time":3.2497789859772
}
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| success | bool | [See error handling](#getting-started-error-handling-error-codes) |
| result | int | Result code |
| result_detail | int | A more detailed response indicating why the result may have failed |
| suggestion | string | If an email address is bad we may return a new, suggested email address based on common misspellings. |

###Result Codes
| Result | Definition |
| -- | -- |
| 0 | Verified Good |
| 1 | Verified Bad |
| 2 | Disposable address |
| 3 | Catch-all |
| 4 | Unknown |

Account
-------
This endpoint allows you to check the balance of credits in your account and see how many jobs are currently running.

>####Example Request
``` bash
$ curl https://api.neverbounce.com/v1/account
    -d key=<API_KEY>
    -d app_id=<APP_ID>
```

###Request Properties
| Property | string | Definition |
| -- | -- |
| app_id | string | API Application ID |
| key | string | API Key |

>####Example Response
``` javascript
{
    "success":true,
    "credits":"35539",
    "jobs_completed":"84",
    "jobs_processing":"0",
    "execution_time":0.74251508712769
}
```

###Response Properties
| Property | Type | Definition |
| -- | -- |
| credit_balance | int | Number of credits remaining in your account |
| jobs_completed | int | Number of jobs completed |
| jobs_processing | int | Number of jobs currently being processed |

Changelog
=========

####Current Version
**Latest version:** v1.1

**Documentation updated:** November 12, 2014

####Changes since last version:
* No changes to report

####Documentation Updates:
* Added PHP examples to reflect our PHP library
* Added result codes to Single method.