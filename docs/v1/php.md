<p class="info" style="background: #c4351e"><span class="fa fa-exclamation-triangle"></span>&nbsp;&nbsp;These docs are no longer updated! Visit our new docs <a href="https://neverbounce.com/help/api/getting-started-with-the-api/" style="color: white; text-decoration: underline;">here</a>.</p>

Getting started
===============
We have introduced a PHP library for easy intergration with your existing PHP applications. This library follows the PSR-4 standards and is available via composer (see example to the right.) It requires both the cURL and JSON PHP extensions to be installed.

You can find the package [here.](https://github.com/creavos/NeverBounceAPI-PHP)

>####Installation with Composer
``` php
composer require "neverbounce/neverbounce-php":*
```

Authentication
--------------
With our PHP library, authenticating yourself is done by passing your Secret Key and APP ID into the class when you first initialize it. (See example to the right.)

The instance will use the same secretKey and appID for every request made with it.

Not supplying your secretKey and appID will result in the library throwing an exception.

``` php
new \NeverBounce\API\Single($secretKey, $appID);
```

Error Handling
--------------
The library does not throw an exception when an error is encountered. Instead, it returns the error information in the response object in the same way that a standard cURL request would do so.

>####Example Request

``` php
$app = new \NeverBounce\API\Single($secretKey, $appID);
$app->Single("support@neverbounce.com");
var_dump($app->response);
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| success | bool | FALSE. Indicates failure |
| error_code | int | A general error code indicating reason for failure |
| error_msg | string | A human-readable explanation/indication of the error. |

>####Example Response

``` php
object(stdClass)#3 (4) {
  ["success"]=>bool(false)
  ["error_code"]=>int(4)
  ["error_msg"]=>string(27) "Insufficent credit balance."
  ["execution_time"]=>float(0.33452916145325)
}
```

###Error Codes
| Error Code | Definition |
| -- | -- |
| 0 | Internal API error |
| 1 | Authorization failure. Check that your APP ID and API Key are correct. |
| 2 | Malformed request. Refer to documentation below for proper formatting |
| 3 | Invalid Job |
| 4 | Insufficient credits. You do not have enough credits in your account to complete the request. |

Methods
=======
Single
------
This endpoint is used to validate single email addresses.

Decoded responses are accessible in the response object. Raw responses are available in the response_raw object. We also provide a number of constants in the class to check results against.

>####Example Request

``` php
$app = new \NeverBounce\API\Single($secretKey, $appID);
$app->verify("support@neverbounce.com");
var_dump($app->response);
```

### Request Properties
| Property | Type | Definition |
| -- | -- | -- |
| email | string | The email address to validate |
| name_f (optional) | string | The first name associated with this email address |
| name_l (optional) | string | The last name associated with this email address |

>####Example Response
``` php
object(stdClass)#3 (4) {
  ["success"]=>bool(true)
  ["result"]=>int(0)
  ["result_detail"]=>int(0)
  ["execution_time"]=>float(4.3265888690948)
}
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| success | bool | [See error handling](#getting-started-error-handling-error-codes) |
| result | int | Result code |
| result_detail | int | A more detailed response indicating why the result may have failed |
| suggestion | string | If an email address is bad we may return a new, suggested email address based on common misspellings. |

###Result Codes
| Constant | Result | Definition |
| -- | -- | -- |
| GOOD | 0 | Verified Good |
| BAD | 1 | Verified Bad |
| DISPOSABLE | 2 | Disposable address |
| CATCHALL | 3 | Catch-all |
| UNKNOWN | 4 | Unknown |

Account
-------
This endpoint allows you to check the balance of your credits and see how many jobs you are running.

>####Example Request
``` php
$app = new \NeverBounce\API\Account($secretKey, $appID);
$app->Account();
var_dump($app->response);
```

###Request Properties
| Property | string | Definition |
| -- | -- | -- |
| app_id | string | API Application ID |
| key | string | API Key |

>####Example Response
``` php
object(stdClass)#3 (5) {
  ["success"]=>bool(true)
  ["credits"]=>string(7) "1000000"
  ["jobs_completed"]=>NULL
  ["jobs_processing"]=>NULL
  ["execution_time"]=>float(0.36210298538208)
}
```

###Response Properties
| Property | Type | Definition |
| -- | -- |
| credit_balance | int | Number of credits remaining in your account |
| jobs_completed | int | Number of jobs completed |
| jobs_processing | int | Number of jobs currently being processed |

Changelog
=========

####Current Version
**Latest version:** v1.1

**Documentation updated:** November 12, 2014

####Changes since last version:
* No changes to report

####Documentation Updates:
* Added PHP examples to reflect our PHP library
* Added result codes to Single method.