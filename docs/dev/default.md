Getting Started
===============

The NeverBounce API is a common RESTful API. All requests are performed via an HTTPS connection and responses are returned as JSON. This widely-accepted format allows for easy use with the most common programming languages and development kits while remaining portable, expandable, and secure.

Below you will find a description of the various endpoints, the parameters they accept, and the responses they send.

``` bash
https://<route>.neverbounce.com/v3
```

Router
------

The router enables us to set up specific environments for special use cases. Unless otherwise noted by our engineers you should use `api` as the router.

``` bash
https://api.neverbounce.com/v3
```

Authentication
--------------
Starting with version 3 we have introduced OAuth 2.0 to the API's authentication.
In order to get started with the API you first have to make a request for an access token to pass along with your requests. To do this make a request to the ``access_token`` endpoint, passing your ``app_id`` and ``api_key`` as the HTTP basic auth credentials. Pass the grant type and scope as post data.

>####Example Request
``` bash
$ curl -X POST -u <APP_ID>:<API_KEY> https://<route>.neverbounce.com/v3/access_token
    -d grant_type=client_credentials
    -d scope=basic+user
```

Once this ``access_token`` is obtained you will pass this along with all requests as the ``access_token`` parameter.

>####Example Response
``` javascript
{
    "access_token":"<ACCESS_TOKEN>",
    "expires_in":3600,
    "token_type":"Bearer",
    "scope":"basic user"
}
```

Versioning
----------

Specific versions of the API can be targeted with by supplying the a `version` parameter with each request. Without the `version` parameter the version `3.0` will be used.

>####Example Request
The following example will demonstrate the error handling. Note the exclusion of our ``access_token``.
``` bash
$ curl -X POST https://<route>.neverbounce.com/v3/single
    -d version=3.1
    -d email=support@neverbounce.com
```

###Versions
* `3.0` (default)
* `3.1`

Error Handling
--------------
Any response coming from the NeverBounce API will include a ‘success’ property, except in the case of the ``access_token`` endpoint. The ``access_token`` endpoint will return a standard OAuth 2.0 error code. If no errors occurred during the process, this property will be set to TRUE. If this property is set to FALSE this indicates that an error has occurred.
If an error has occurred, an ``error_code`` and ``error_msg`` will also be included in the response.

>####Example Request
The following example will demonstrate the error handling. Note the exclusion of our ``access_token``.
``` bash
$ curl -X POST https://<route>.neverbounce.com/v3/single
    -d email=support@neverbounce.com
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| success | bool | FALSE. Indicates failure |
| error_code | int | A general error code indicating reason for failure |
| error_msg | string | A human-readable explanation/indication of the error. |

>####Example Response

``` javascript
{
    "success":false,
    "error_code":2,
    "error_msg":"Missing required parameter 'access_token'",
    "execution_time":2.288818359375e-5
}
```

###Error Codes
| Error Code | Definition |
| -- | -- |
| 0 | Internal API error |
| 1 | Authorization failure. Check that your APP ID and API Key are correct. |
| 2 | Malformed request. Refer to documentation below for proper formatting |
| 3 | Invalid Job |
| 4 | Insufficient credits. You do not have enough credits in your account to complete the request. |

Methods
=======
Single
------
The single validation endpoint is used to validate one email address at a time, rather than a list of many.

### Request Properties
| Property | Type | Definition |
| -- | -- | -- |
| access_token | string | Access token returned from `access_token` endpoint |
| email | string | The email address to validate |
| name_f (optional) | string | The first name associated with this email address |
| name_l (optional) | string | The last name associated with this email address |

>####Example Request
``` bash
$ curl -X POST https://<route>.neverbounce.com/v3/single
    -d access_token=<ACCESS_TOKEN>
    -d email=<EMAIL>
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| success | bool | [See error handling](#getting-started-error-handling-error-codes) |
| result | int | Result code |
| result_details | int | A more detailed response indicating why the result may have failed |
| suggestion | string | If an email address is bad we may return a new, suggested email address based on common misspellings. |

>####Example Response
``` javascript
{
    "success":true,
    "result":0,
    "result_details":0,
    "execution_time":1.2497789859772
}
```

###Result Codes
| Result | Definition |
| -- | -- |
| 0 | Valid |
| 1 | Invalid |
| 2 | Disposable address |
| 3 | Catch-all |
| 4 | Unknown |

Account
-------
This endpoint allows you to check the balance of credits in your account and see how many jobs are currently running.

###Request Properties
| Property | string | Definition |
| -- | -- |
| access_token | string | Access token returned from `access_token` endpoint |

>####Example Request
``` bash
$ curl -X POST https://<route>.neverbounce.com/v3/account
    -d access_token=<ACCESS_TOKEN>
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| success | bool | [See error handling](#getting-started-error-handling-error-codes) |
| credits | int | Number of credits remaining in your account |
| free_api_credits | Integer | The remaining free credits left in your account. (Replenished monthly) |
| monthly_api_usage | Integer | [Monthly Billing] The billable api usage for the current month |
| monthly_dashboard_usage | Integer | [Monthly Billing] The billable dashboard usage for the current month |
| jobs_completed | int | Number of jobs completed |
| jobs_processing | int | Number of jobs currently being processed |

>####Example Response
``` javascript
{
    "success":true,
    "credits":"35539",
    "jobs_completed":"84",
    "jobs_processing":"0",
    "execution_time":0.02251508712769
}
```

Bulk
----

The bulk endpoint provides high speed validation on a list of email addresses. You can use the `status` endpoint to retrieve real time statistics about a bulk job in progress. Once the job has finished, the results can be retrieved with our `download` endpoint.

###Free Analysis

The `bulk` endpoint can now start sample a list and provide you with an estimated bounce rate without costing you. Based on the estimated bounce rate you can decide whether or not to perform the full validation or not. You can start validation by calling the `start_job` endpoint.

###Input & Input Location

The `bulk` endpoint can receive input in multiple ways. The `input_location` parameter describes the contense of the `input` parameter. The `input` parameter may be a list of encoded email addresses, a file hosted at a remote URL, or other resource identifier based on the value of `input_location`. For now, two `input_location` styles are available. Remote URL and Supplied Data.
####Remote URL

Using a remote url allows you to host the file and provide us with a direct link to it. The file should be a list of emails separated with line breaks (\r\n or \n) or a standard CSV file (comma, semi-colon, space or tabs to delimit columns, and line breaks (\r\n or \n) to delimit rows). We support most common file transfer protocols and their related authentication mechanisms. We suggest using either `http`, `https`, `ftp`, `ftps` or `sftp`. When using a url that requires authentication be sure to pass the username and password in the actual url. When using `sftp` keep in mind that we do not currently support key based authentication.

>####Valid URLs
``` bash
# FTP With Authentication
ftp://name:passwd@example.com:21/full/path/to/file
# HTTP Request
http://example.com/full/path/to/file
# HTTP Basic Authentication
http://name:passwd@example.com/full/path/to/file
```

####Suplied Data

Supplying the data directly gives you the option to dynamically create email lists on the fly. `input` will accept a list of emails separated with line breaks (`\r\n` or `\n`) or csv formatted data (comma, semi-colon, space or tabs to delimit columns, and line breaks (`\r\n` or `\n`) to delimit rows). When using this method be sure that the data is also URL encoded to prevent any mangling of the data.

>####Raw text Input (New lines)
``` bash
joe@example.com\n
alice@example.com\n
tom@example.com\n
```

>####Raw CSV Input
``` bash
ID, Email, Name, Department\n
1, joe@example.com, Joe, Accounting\n
2, alice@example.com, Alice, Accounting\n
3, tom@example.com, Tom, Billing\n
```

###Request Properties
| Property | string | Definition |
| -- | -- | -- |
| input_location | Integer | The source of the input data. <br> 0 = Remote URL<br> 1 = Supplied Data |
| input | String | Either a URL to the input or raw data as described by input_location |
| filename | String | The name of the file the job will be run under. |
| run_sample | Boolean | (Optional) Set this to `1` to start the job as a free list analysis. If you do not supply this parameter it's assumed false. |

>####Example Request
``` bash
$ curl -X POST https://<route>.neverbounce.com/v3/bulk
    -d access_token=<ACCESS_TOKEN>
    -d input_location = <INPUT_LOCATION>
    -d input = <INPUT>
    -d filename = <FILENAME>
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| job_id | int | An ID that can be used reference the job to either download the data or check on the job progress. |

>####Example Response
``` javascript
{
    "success": true,
    "job_id": 5257,
    "execution_time": 0.023593902587891
}
```

Start Job
---------

*This endpoint only needs to be called when you want to start a bulk job that was created with the `run_sample` parameter*

This endpoint will start a job already that has already been sent in our system.

###Request Properties
| Property | string | Definition |
| -- | -- | -- |
| job_id | Integer | The ``job_id`` returned by the ``bulk`` endpoint |

>####Example Request
``` bash
$ curl -X POST https://<route>.neverbounce.com/v3/start_job
    -d access_token=<ACCESS_TOKEN>
    -d job_id = <JOB_ID>
```

###Response
This endpoint will return the `success` parameter. Details on this parameter can be found in the [Error Handling](#getting-started-error-handling) section.

>####Example Response
``` javascript
{
    "success": true,
    "execution_time": 0.023593902587891
}
```

Status
------

Used to check on the status of an email list being validated with the ``bulk`` endpoint.

###Request Properties
| Property | string | Definition |
| -- | -- | -- |
| job_id | Integer | The ``job_id`` returned by the ``bulk`` endpoint |

>####Example Request
``` bash
$ curl -X POST https://<route>.neverbounce.com/v3/status
    -d access_token=<ACCESS_TOKEN>
    -d job_id = <JOB_ID>
```

###Response Properties
| Property | Type | Definition |
| -- | -- | -- |
| success | Boolean | True or False, indicates successful negotiation with API. |
| id | Integer | The job's ID |
| user_id | Integer | The user's ID |
| status | Integer | The id representing the job's state |
| type | Integer | Where the job came from <br> 0 = Dashboard <br> 1 = API |
| input_location | Integer | Indicates where the list data came from |
| orig_name | String | The name associated with the list |
| stats | [Stats](#methods-status-stats-object) | Contains the job's validation results |
| created | String | Date and time at when job was created (ISO 8601) |
| started | String | Date and time at when job was started (ISO 8601) |
| finished | String | Date and time at when job was finished (ISO 8601) |
| file_details | [File Details](#methods-status-file-details) | Contains details about the data supplied to the validator. Useful for detecting issues with the data. |
| job_details | [Job Details](#methods-status-job-details) | Contains additional information about the job |

>####Example Request
``` javascript
{
    "success": true,
    "id": "5456",
    "status": "4",
    "type": "0",
    "input_location": "0",
    "orig_name": "list100_c.csv",
    "stats": {
        "total": 101,
        "processed": 101,
        "valid": 25,
        "invalid": 65,
        "bad_syntax": 1,
        "catchall": 3,
        "disposable": 3,
        "unknown": 5,
        "duplicates": 0,
        "billable": 100,
        "job_time": 8
    },
    "created": "2015-03-25 23:28:21",
    "started": "2015-03-25 11:28:48",
    "finished": "2015-03-25 23:29:28",
    "file_details": "{\"email_col_i\":0,\"tot_cols\":22,\"delimiter\":\",\",\"has_header\":true,\"size\":18957,\"tot_records\":102,\"tot_emails\":100}",
    "job_details": {
        "sample_status": 3,
        "sample_details": {
            "sample_size": 100,
            "sample_stats": {
                "total": 1001,
                "processed": 102,
                "valid": 33,
                "invalid": 55,
                "bad_syntax": 1,
                "catchall": 4,
                "disposable": 0,
                "unknown": 10,
                "duplicates": 0,
                "billable": 1000,
                "percent_complete": 10
            },
            "bounce_estimate": 0.6
        }
    },
    "execution_time": 0.15184903144836
}
```

### Stats Object

| Property | Type | Definition |
| -- | -- | -- |
| total | Integer | Total number of emails to process in list |
| processed | Integer | Total number of emails processed |
| valid | Integer | Total number of valid emails |
| invalid | Integer | Total number of invalid emails |
| bad_syntax | Integer | Total number of emails marked invalid because of bad syntax (no at symbol, missing domain, etc...). These are also counted in `invalid` parameter |
| catchall | Integer | Total number of catch-all emails |
| disposable | Integer | Total number of disposable emails |
| unknown | Integer | Total number of unknown emails |
| duplicates | Integer | Total number of duplicate emails, when a duplicate is found we only validate it once. |
| billable | Integer | Total number of billable emails. This is how many credits the job costs to run. `bad_syntax` and `duplicates` do not count toward this. |
| job_time | Integer | Time in seconds it took for the job to complete |

### Statuses

| code | Description |
| -- | -- |
| -1 | UPLOADING: Job uploading, or transmission in progress. |
| 0 | RECEIVED:  Job was received successfully and has entered the system queue. |
| 1 | PARSING: We have started parsing your file to determine totals, detect bad data, and duplicate entries. |
| 2 | PARSED: Job parsing has completed, this job is ready to be ran. (Running will start automatically) |
| 3 | RUNNING: Job is currently in progress. |
| 4 | COMPLETED: Job has successfully completed, download is available. |
| 5 | FAILED: An unexpected failure has occurred, this job could not be ran. (typically due to bad CSV data) |

### File Details
| Property | Type | Definition |
| -- | -- | -- |
| email_col_i | Integer | Zero based index of what column the emails where found |
| tot_cols | Integer | Number of columns we found in the CSV |
| delimiter | String | The delimiter we detected from the CSV |
| has_header | Boolean | Whether or not we detected a header |
| size | Integer | The size of the file or data supplied in bytes |
| tot_records | Integer | Total number of records detected in CSV |
| tot_emails | Integer | Total number fo emails found in the CSV |

### Job Details
| Property | Type | Definition |
| -- | -- | -- |
| sample_status | [Sample Status](#methods-status-sample-status) | The status of the sample |
| sample_details | [Sample Details](#methods-status-sample-details) | The results from the sample |

### Sample Status
| Property | Definition |
| -- | -- |
| 1 | Indicates we have received the request to run the job as a sample. |
| 2 | We are currently processing your free sample job. |
| 3 | We have finished processing your sample job. |
| 4 | We have finished processing your sample job. |

### Sample Details
| Property | Type | Definition |
| -- | -- | -- |
| sample_size | Integer | he number of records chosen to test for a free sample (typically 10% of the total) |
| sample_stats | [Stats](#methods-status-stats-object) | Contains the job's analysis results |
| bounce_estimate | Float | The estimated bounce rate for this list |

Download
--------

*Jobs started with the `run_sample` parameter can not be downloaded until they have completed validation after calling `start_job`*

Used to retrieve the data once a list has completed validation. This endpoint returns the data back in the same format you supplied it with an additional column indicating if what the email is. If you only specify a `job_id` without any of the optional parameters they will be enabled. If you specify optional parameters any parameter not specified will be disabled.

###Request Properties
| Property | string | Definition |
| -- | -- | -- |
| job_id | Integer | The ``job_id`` returned by the ``bulk`` endpoint |
| valids | Integer | Return valids (optional) |
| invalids | Integer | Return invalids (optional) |
| catchall | Integer | Return catchalls (optional) |
| disposable | Integer | Return disposable (optional) |
| unknowns | Integer | Return unknowns (optional) |
| duplicates | Integer | Return duplicates (optional) |
| textcodes | Integer | Return status codes as readable text or as integers (optional) |

>####Example Request
``` bash
# Download all emails & duplicates with textcodes
$ curl -X POST https://<route>.neverbounce.com/v3/download
    -d access_token=<ACCESS_TOKEN>
    -d job_id = <JOB_ID>
```
``` bash
# This returns the same data
$ curl -X POST https://<route>.neverbounce.com/v3/download
    -d access_token=<ACCESS_TOKEN>
    -d job_id = <JOB_ID>
    -d valids=1
    -d invalids=1
    -d catchall=1
    -d disposable=1
    -d unknowns=1
    -d duplicates=1
    -d textcodes=1
```
``` bash
# Download just valids & catchalls without duplicates
$ curl -X POST https://<route>.neverbounce.com/v3/download
    -d access_token=<ACCESS_TOKEN>
    -d job_id = <JOB_ID>
    -d valids=1
    -d catchall=1
    -d textcodes=1
```

Changelog
=========

####Current Version
**Latest version:** v3.1

**Documentation updated:** July 31, 2015

####Changes:

######July 31, 2015
* Added `versioning` section

######July 29, 2015
* Updated the `sample details`

######July 28, 2015
* Updated API Version to 3.1
* Added documentation for Free Analysis
* Added `start_job` endpoint
* Updated `bulk`, `status`, `download`, `account`
* Added additional definitions to `status`

######April 22, 2015
* Updated several examples with `-X POST` argument
* Added example to `Download` endpoint
* Added status code definitions to the `Status` endpoint

######April 17, 2015
* Added `Router` section

######March 31, 2015
* Updated the `bulk`, `status` and `download` sections.
* Added examples to `bulk`, `status` and `download` sections.
* Minor corrections to examples.
* Minor corrections to document verbiage.

######March 16, 2015
* Updated the `bulk` section.
* Updated `key` to `access_token`

######March 12, 2015
* Fixed examples in documentation, they were still showing v1 style authentication.

######March 3, 2015
* Starting with v3 OAuth 2.0 has been introduced
* Specifications for ``bulk``, ``status`` and ``download`` added

Contact
=======

Contact **support@neverbounce.com** to get started with the API.<br/>
Report any technical issues or bugs using our [helpdesk](http://help.neverbounce.com)