module.exports = function(grunt) {

    // Define the configuration for all the tasks
    grunt.initConfig({
        // Watches files for changes and runs tasks based on the changed files
        watch: {
            tasks: ['sass'],
            options: {
                livereload: true
            },
            files: [
                '{,*/}*.html',
                '{,*/}*.md',
                'scss/{,*/}*.scss',
                'images/{,*/}*'
            ]
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 9090,
                open: true,
                livereload: 35729,
                // Change this to '0.0.0.0' to access the server from outside
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    middleware: function(connect) {
                        return [
                            connect.static('.')
                        ];
                    }
                }
            }
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            dev: {
                files: [{
                    expand: true,
                    cwd: 'scss/',
                    src: ['*.{scss,sass}'],
                    dest: 'css/',
                    ext: '.css'
                }]
            }
        },

        csscomb: {
            options: {
                config: '.csscomb.json'
            },
            dynamic_mappings: {
                expand: true,
                cwd: 'css/',
                src: ['*.css'],
                dest: 'css/',
                ext: '.css'
            }
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']
            },
            dev: {
                files: [{
                    expand: true,
                    cwd: 'css/',
                    src: '{,*/}*.css',
                    dest: 'css/'
                }]
            }
        },
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-csscomb');


    grunt.registerTask('default', ['connect:livereload','watch']);

};