<?php
/**
 * API Documentation
 */

$versions = [
//    'v1.0' => [
//        'cURL' => 'docs/v1/default.md',
//        'PHP' => 'docs/v1/php.md'
//    ],
//    'v2.0' => [
//        'cURL' => 'docs/v2/default.md'
//    ],
    'v3.0' => [
        'cURL' => 'docs/v3/default.md',
    ],
    'dev' => [
        'cURL' => 'docs/dev/default.md'
    ]
];

if(!isset($_GET['version']) || !isset($versions[$_GET['version']]))
    $version = 'v3.0';
else
    $version = $_GET['version'];

if(!isset($_GET['language']) || !isset($versions[$_GET['version']][$_GET['language']]))
    $language = 'cURL';
else
    $language = $_GET['language'];
?>
<!doctype html>
<html>
<head>
    <meta charset='utf-8'>
    <title>NeverBounce | Api Documentation</title>
    <link rel="stylesheet" href="/css/main.css"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>
<body role='flatdoc'>
<div class='header'>
    <div class='left'>
        <ul class="header-nav">
            <li class="logo"><a href="https://neverbounce.com" style="line-height: 0"><img src="/images/nb_logo-300x23.png" alt="NeverBounce logo"/></a></li>
        </ul>
    </div>
    <div class='right'>
        <ul>
            <li><a href="http://help.neverbounce.com">Help Docs</a></li>
            <li><a href="https://app.neverbounce.com">Dashboard</a></li>
            <!--<li><a href='https://github.com/USER/REPO'>View on GitHub</a></li>-->
            <!--<li><a href='https://github.com/USER/REPO/issues'>Issues</a></li>-->
        </ul>
        <!-- GitHub buttons: see http://ghbtns.com -->
    </div>
</div>

<div class='content-root'>
    <div class='menubar'>
        <div class='menu section' role='flatdoc-menu'></div>
    </div>
    <div role='flatdoc-content' class='content'></div>
</div>

<!-- Flatdoc -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src='//cdn.rawgit.com/rstacruz/flatdoc/v0.9.0/flatdoc.js'></script>
<script src='/scripts/main.js'></script>

<!-- Initializer -->
<script>
    Flatdoc.run({
        fetcher: Flatdoc.bitbucket('creavos/nbdocs', '<?php echo $versions[$version][$language]?>', 'master')
    });
</script>
</body>
</html>