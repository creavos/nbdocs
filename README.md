# NeverBounce Docs#

[docs.neverbounce.com](http://docs.neverbounce.com)
This site is using the [FlatDoc](http://ricostacruz.com/flatdoc/) library. It allows us to keep our documentation in markdown files hosted on bitbucket/github, it does some magic and converts the markdown to html.

### Editing ###

*Install NodeJS prior to editing*

To edit the content you'll want to open up the terminal and navigate to the `app` directory in the repository. If this is your first time here, run `npm install`. Once NodeJS has installed the required packages run `grunt default`. This will start a livereload server using your default browser. From here you can edit the `default.md` file in this directory. As you save this file the browser will refresh on it's own displaying the changes.

For help with markdown syntax [click here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). You may notice that this markdown file also contains some HTML, this is fine as long as you don't mix in html into markdown or markdown into html ([gotta keep them seperated](https://www.youtube.com/watch?v=XN32lLUOBzQ)). Unless necessary always use markdown, as it is easier to follow when viewed in plaintext.

When you're ready to make the edits live, simply copy the contents of the `default.md` file to the desired production file (view below).

### Production ###

The current latest version of the doc is found in `docs/v3/default.md`.